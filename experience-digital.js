var selections = [];
var selection_container = $('.selectionContainer');
var COLORS = JSON.parse(("{'Wineberry':'#85252a','Red-Red':'#c0403d','Crimson':'#e34248','Paprika':'#d82520','Flame':'#e63500','Tangerine':'#fc4c02','Ruby-Red':'#c5024d','Coral':'#e0457b','Magenta':'#da1984','Mahogany':'#653024','Brown':'#603d20','Russet':'#a14737','Cocoa':'#6d4f47','Sapphire':'#23249e','Blueberry':'#0065a4','Blackberry':'#6e334d','Peach':'#fdbe87','Ochre':'#b47e00','Marigold':'#fccd00','White':'#ffffff','Black':'#000000'}").replace(/'/g,'"'));

// Recupera il valore esadecimale senza # davanti
function cutHex(h) {return (h.charAt(0)=="#") ? h.substring(1,7):h}

// Prende la componente rossa (R) dall'esadecimale
function hexToR(h) {return parseInt((cutHex(h)).substring(0,2),16)}

// Prende la componente verde (G) dall'esadecimale
function hexToG(h) {return parseInt((cutHex(h)).substring(2,4),16)}

// Prende la componente blu (B) dall'esadecimale
function hexToB(h) {return parseInt((cutHex(h)).substring(4,6),16)}

// Converte input in esadecimale (base 16); l'input atteso è la singola componente di colore (R, G o B)
function cToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

// Converte un rgb in esadecimale
function rgbToHex(rgb) {
    return "#" + cToHex(rgb.r) + cToHex(rgb.g) + cToHex(rgb.b);
}

// Calcola il colore medio data la lista di colori e il loro peso/percentuale e ritorna l'esadecimale
var getAverage = function(list) {
    var rgb_total = { r: 0, g: 0, b: 0};
    var weight_total = 0;
    _.each(list, function(l) {
        rgb_total.r = rgb_total.r + hexToR(l.color) * l.weight;
        rgb_total.g = rgb_total.g + hexToG(l.color) * l.weight;
        rgb_total.b = rgb_total.b + hexToB(l.color) * l.weight;
        weight_total = weight_total + l.weight;
    });
    var rgb = {
        r: ~~(rgb_total.r/weight_total),  // ~~ è un sostituto più veloce per Math.floor() (per quanto riguarda i numeri positivi)
        g: ~~(rgb_total.g/weight_total),
        b: ~~(rgb_total.b/weight_total)
    }
    return rgbToHex(rgb)
};

$( function() {
    // Calcola l'esadecimale date le singole componenti di colore
    function hexFromRGB(r, g, b) {
        var hex = [
            r.toString( 16 ),
            g.toString( 16 ),
            b.toString( 16 )
        ];
        $.each( hex, function( nr, val ) {
            if ( val.length === 1 ) {
                hex[ nr ] = "0" + val;
            }
        });
        return hex.join( "" ).toUpperCase();
    }

    // Modifica la grafica se viene modificato lo slider di un colore
    function refreshSwatch() {
        var red = $( "#red" ).slider( "value" ),
            green = $( "#green" ).slider( "value" ),
            blue = $( "#blue" ).slider( "value" ),
            hex = hexFromRGB( red, green, blue );
        $( ".customizationBackground" ).css( "background-color", "#" + hex );
        $( ".customizationBackground" ).css( "mix-blend-mode", multiply );
        $( ".under-lipstick" ).css( "opacity", "0.3");
        $( "#swatch" ).css( "fill", "#" + hex );
        $("#inputColorCode").val('#'+hex);
      	console.log('test color change2 #'+hex);

    }

    // Inizializzazione degli slider per i tre colori
    $( "#red, #green, #blue" ).slider({
        orientation: "horizontal",
        range: "min",
        max: 255,
        value: 127,
        slide: refreshSwatch,
        change: refreshSwatch
    });
    $( "#red" ).slider( "value", 255 );
    $( "#green" ).slider( "value", 140 );
    $( "#blue" ).slider( "value", 60 );
} );

// Click dei singoli colori allo step 1
// Aggiunge / rimuove colore dalla lista dei selezionati, prepara lo slider per lo step 2
$(document).on("click",".colorContainer", function() {
    var prop = $(this).find('.colorCard');
    if ($(this).find('.colorCard').hasClass('active')) {
        $(this).find('.colorCard').removeClass('active');
        selections = _.reject(selections, { name: $(prop).data('value') });

    } else {
        if(selections.length < 4) {
            var name = $(this).attr('data-name');
            selections.push({ name: $(prop).data('value'), c_name: $(prop).data('value'), percentage: 100, color: COLORS[$(prop).data('value')] });
            selections = _.uniq(selections, 'name');
            $(this).find('.colorCard').addClass('active');
        }
    }
    // console.log(selections);
    addSlider();
});
$(document).ready(function(){
    // Aggiunge dinamicamente i colori da COLORS
    _.each(COLORS, function(val, key){
      console.log("val color: "+val+" key color: "+key);
        $('.refColorCard').find('.colorCard').css({ 'background-color': val });
        $('.refColorCard').find('.colorCard').data('value', key);
        var tempData = $(".refColorCard").html();
        var newData = tempData.replace(new RegExp('{name}', 'g'), key);
        newData = newData.replace(new RegExp('{lower_name}', 'g'), key.toLowerCase());
        newData = newData.replace(new RegExp('{color}', 'g'), val);
        ($(".colorBoxContainer").append(newData))
    });
    render_result()

    $("#customForm").submit(function(e) {

        if($('#inputColorCode').val()) {

        } else {
            e.preventDefault();
            alert('Please Select Atleast One Color')
        }
    });
});

// Gestisce la visualizzazione dei colori scelti e presenti in selections
function renderTopColors() {
    $('.topColorSelector').find('h3').css('color', 'white');
    // Prende tutti e 4 i possibili colori
    [0,1,2,3].forEach(function (index)  {
        var color = selections.length >= (index+1) ? selections[index].color : ' transparent';
        console.log("color selected: "+color);
      	var colorName = selections.length >= (index+1) ? selections[index].name : '';
      	console.log("color selected name: "+colorName);
        $(".color"+(index+1)).css('background-color', color);   // Mette colore nell'immagine del rossetto
      	$(".color"+(index+1)).attr('data-name', colorName);
      	$(".Step1colorName"+(index+1)).html(colorName);   // Mette nome nell'etichetta sotto il rossetto
      	$(".colorName"+(index+1)).html(colorName);    // Mette nome nell'etichetta allo step 2 (quello con gli slider)
      	$('#inputColor'+(index+1)).val(color);
        if (selections.length >= index+1) {
            if (selections[index].color == '#ffffff') {
                var count = index;
                // alert(index);
                $(".color" + (index + 1)).find('h3').css("color", 'black');
            } else {
                $(".color" + (index + 1)).find('h3').css("color", 'white');
            }
        }
        if (selections.length == 0) {
            $( ".customizationBackground" ).css( "fill", 'rgb(255, 255, 255, 0)' );
            $( ".customizationBackground" ).css( "mix-blend-mode", 'multiply' );
            $( ".under-lipstick" ).css( "opacity", "0.3");
        }
    });
}

// Gestione e visualizzazione dei colori selezionati
var render_result = function() {
    renderTopColors();
    $(".hiddenInputs").html(' ');
    if (selections.length == 0) {
        $('.resultMainContainer').hide();
        $('.resultContainer').hide();
        return;
    }
    $('.resultMainContainer').show();
    $('.resultContainer').show();
    var listData = '';
    var hex_list = _.map(selections, function(s,index){
        listData += "<input type='hidden' id='inputColor"+index+"' name='color"+index+"' value='"+s.c_name+','+s.percentage+"%' />";
        return { color: COLORS[s.name], weight: s.percentage/100 }
    });
    $(".hiddenInputs").html(listData);
    var renderedcode = getAverage(hex_list);
    $( "#swatch" ).css( "background-color", renderedcode );   // colore nel rossetto (visibile da mobile)
    $("#inputColorCode").val(renderedcode);
  	console.log('test color change1'+renderedcode);
    $( ".customizationBackground" ).css( "background-color", renderedcode );  // div rossetto
    $( ".customizationBackground" ).css( "fill", renderedcode );  // div rossetto
    $( ".circle-result" ).css( "background-color", renderedcode );  // div cerchio mix
    $( ".under-lipstick" ).css( "opacity", "0.3");
  	$('.Product__SlideItem--image').eq('0').find('img').css('background-color', renderedcode);  // div delle labbra dello step 3
  	$('.Product__SlideshowNavImage').eq('0').find('img').css('background-color', renderedcode);
	
}

// Inizializzazione degli slider allo step 2 dei colori scelti + gestione della percentuale e della modifica
var convert_to_slider = function() {
  
    selection_container.find('.slider').each(function() {
        var el = $(this);
        
        var s = _.find(selections, { name: el.data('name') });
        selection_container
            .find('[data-container="percentage"][data-name="' + s.name + '"]')
            .text(s.percentage + '%');
        el.slider({
            orientation: "horizontal",
            range: "min",
            max: 100,
            value: s.percentage,
            step: 10,
            change: function() {
                var val = el.slider('value');
                s.percentage = val;
                selection_container
                    .find('input[data-name="' + s.name + '"]')
                    .val(s.name + ' ' + s.percentage + '%');

                selection_container
                    .find('[data-container="percentage"][data-name="' + s.name + '"]')
                    .text(s.percentage + '%');

                render_result();
            }
        });
        el.find('.ui-slider-range').css({'background': COLORS[s.name]});
        $(el).find('.ui-slider-handle ').css({ 'background': COLORS[s.name] });
    });
}

// Preparazione degli slider allo step 2
function addSlider() {
    render_result();
    selection_container.html('<span class="resultHeading"></span>');
    _.each(selections, function(selection, i) {
         console.log(selection+i);
        var tempData = $(".refSlider").html();
        var newData = tempData.replace(new RegExp('{name}', 'g'), selection.name);
        newData = newData.replace(new RegExp('{color}', 'g'), selection.color);
        newData = newData.replace(new RegExp('{percentage}', 'g'), selection.percentage);
        if(i == 0){
			($(".resultMainContainer").find('.firstFlex .selectionContainer').append(newData))
        } 
      	if(i == 1){
			($(".resultMainContainer").find('.secondFlex .selectionContainer').append(newData))
        }
      	if(i == 2){
			($(".resultMainContainer").find('.thirdFlex .selectionContainer').append(newData))
        }
      	if(i == 3){
			($(".resultMainContainer").find('.fourthFlex .selectionContainer').append(newData))
        }
        //($(".selectionContainer").append(newData))
    });
    convert_to_slider()
}

// Inizializzazione carousel
$('.owl-carousel').owlCarousel({
    loop: false,
    margin: 0,
    nav: true,
    autoplay: false,
    autoplayHoverPause: false,
    lazyLoad:false,
    mouseDrag: false,
    touchDrag: false,
    autoHeight:true,
    dots:false,
    // animateOut: 'fadeOut',

    // animateOut: 'slideOutDown',
    // animateOut: 'fadeIn',
    animateIn: 'slideInUp',
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 1
        },
        1000: {
            items: 1
        }
    }
});

//Ombra colori al select e nascosta lineetta sotto 4 tondi

const colorContainer = document.querySelectorAll(".colorContainer");
const colorsBoxes = document.querySelectorAll(".colorBorderExternal");
const underSelector = document.querySelector(".underSelector");
console.log("colorsBoxes", colorsBoxes);
for (const colorBox of colorsBoxes) {
    console.log("colorBox", colorBox)
    colorBox.addEventListener('click', function handleClick() {
        colorBox.classList.toggle('active');
        $('#color-first-step').hide();
        $('#intensity-first-step').css({ 'opacity': '1' });
        $('.underSelector').css({'display': 'none'});
    });
}


// colorCard.addEventListener("click", function () {
//     colorContainer.style.background = "white";
//     colorContainer.style.border = "1px solid #4C4672";
//     colorContainer.style.borderRadius = "100px";
//     colorContainer.style.boxShadow = "3px 0px 9px rgb(79 102 167 / 65%)";
//     underSelector.style.display = "none";
//     selectColor.style.display = "none";
//     buttonIntensity.style.display = "inline-block";
//  });

/*const plus = document.querySelector(".plus");
const addPlus = document.querySelector(".add-plus");
const underSelectorHidden = document.querySelector(".underSelector.hidden");
const closeButton = document.querySelector(".closeButton");

plus.addEventListener("click", function () {
    plus.style.opacity = "1";
    addPlus.style.display = "none";
    underSelectorHidden.style.visibility = "visible";
    closeButton.style.visibility = "visible";
    buttonIntensity.style.display = "none";
    selectColor.style.display = "inline-block";
 });

closeButton.addEventListener("click", function () {
    plus.style.opacity = "0.3";
    underSelectorHidden.style.visibility = "hidden";
    closeButton.style.visibility = "hidden";
});
*/
const buttonShowTexture = document.getElementById('buttonShowTexture');
const buttonHideTexture = document.getElementById('buttonHideTexture');


if (document.querySelector('input[name="texture"]')) {
    document.querySelectorAll('input[name="texture"]').forEach((elem) => {
      elem.addEventListener("click", function(event) {
        buttonShowTexture.style.opacity = '1';
        buttonHideTexture.style.display = 'none';
    });
  });
}

const buttonShowPrincipio = document.getElementById('buttonShowPrincipio');
const buttonHidePrincipio = document.getElementById('buttonHidePrincipio');

if (document.querySelector('input[name="principio"]')) {
    document.querySelectorAll('input[name="principio"]').forEach((elem) => {
      elem.addEventListener("click", function(event) {
        buttonShowPrincipio.style.opacity = '1';
        buttonHidePrincipio.style.display = 'none';
    });
  });
}

const buttonShowAroma = document.getElementById('buttonShowAroma');
const buttonHideAroma = document.getElementById('buttonHideAroma');

if (document.querySelector('input[name="aroma"]')) {
    document.querySelectorAll('input[name="aroma"]').forEach((elem) => {
      elem.addEventListener("click", function(event) {
        buttonShowAroma.style.opacity = '1';
        buttonHideAroma.style.display = 'none';
    });
  });
}

const buttonShowColaggio = document.getElementById('buttonShowColaggio');
const buttonHideColaggio = document.getElementById('buttonHideColaggio');

if (document.querySelector('input[name="colaggio"]')) {
    document.querySelectorAll('input[name="colaggio"]').forEach((elem) => {
      elem.addEventListener("click", function(event) {
        buttonShowColaggio.style.opacity = '1';
        buttonHideColaggio.style.display = 'none';
    });
  });
}



// Gestione del passaggio dallo step 1 allo step 2: visualizzazione degli slider necessari, modifica delle etichette
$( "#stepOneNext" ).on('click', function (e) {
    if( selections.length < 1){
        e.preventDefault();
        alert('Please select atleast One Color Shade!');
    } else{
        if (selections.length == 0) {
            $( ".customizationBackground" ).css( "background-color", 'transparent' );
            $( ".customizationBackground" ).css( "mix-blend-mod", 'multiply' );
            $( ".firstSelection" ).css( "display", 'none' );
            $( ".secondSelection" ).css( "display", 'none' );
            $( ".thirdSelection" ).css( "display", 'none' );
            $( ".forthSelection" ).css( "display", 'none' );
        }
        if (selections.length == 1) {
            $( ".firstSelection" ).css( "display", 'flex' );
            $( ".secondSelection" ).css( "display", 'none' );
            $( ".thirdSelection" ).css( "display", 'none' );
            $( ".forthSelection" ).css( "display", 'none' );
        }
        if (selections.length == 2) {
            $( ".firstSelection" ).css( "display", 'flex' );
            $( ".secondSelection" ).css( "display", 'flex' );
            $( ".thirdSelection" ).css( "display", 'none' );
            $( ".forthSelection" ).css( "display", 'none' );
        }
        if (selections.length == 3) {
            $( ".firstSelection" ).css( "display", 'flex' );
            $( ".secondSelection" ).css( "display", 'flex' );
            $( ".thirdSelection" ).css( "display", 'flex' );
            $( ".forthSelection" ).css( "display", 'none' );
        }
        if (selections.length == 4) {
            $( ".firstSelection" ).css( "display", 'flex' );
            $( ".secondSelection" ).css( "display", 'flex' );
            $( ".thirdSelection" ).css( "display", 'flex' );
            $( ".forthSelection" ).css( "display", 'flex' );
        }


        owl.trigger('next.owl.carousel');   // il passaggio tra gli step viene fatto con il carousel
      	var headpart1_2= '', subheadpart1_2= '', finalpart1_2= '';
      	var hiddenhead1_2= $('#step2_head').val();
      	var hiddensubhead1_2= $('#step2_subhead').val();
      	console.log("second step hidden is: "+hiddenhead1_2);
      	if(hiddenhead1_2 != 'NAN'){
          headpart1_2= hiddenhead1_2;
        } 
      	if(hiddensubhead1_2 != 'NAN'){
          subheadpart1_2= hiddensubhead1_2;
        }
        $('.textCustomiseChange').html(headpart1_2);
      	$('.textCustomiseSubhead').html(subheadpart1_2);
        $('.customisationCardNo').html('STEP 2 ');
        $('.nextCustomisationCardNo').html('STEP 3');
        $('.step1Navigation').hide();
        $('.step2Navigation').show();
      	$('html, body').animate({
          scrollTop: $("#main").offset().top
        }, 1000);
    }
});

$('#intensity-first-step,#tonality-second-step,#buttonShowTexture,#buttonShowPrincipio,#buttonShowAroma,#no-custom-pack,#buttonShowColaggio').click(function() {
    console.log($('.owl-next'));
    //owl.trigger('owl.next');
    $('.owl-next').trigger('click');
})

$('#intensity-first-step').click(function() {
    $('.mix-color-result').css({ 'display': 'flex' });
    $('.lipstickImginside').css({ 'display': 'none' });
});
$('#tonality-second-step').click(function() {
    $('.mix-color-result').css({ 'display': 'none' });
    $('.lipstickImginside').css({ 'display': 'none' });
    $('.lips-texture').css({ 'display': 'flex' });
});
$('#buttonShowPrincipio').click(function() {
    $('.mix-color-result').css({ 'display': 'none' });
    $('.lipstickImginside').css({ 'display': 'none' });
    $('.lips-texture').css({ 'display': 'none' });
    $('.lips-aroma').css({ 'display': 'flex' });
});
$('#nessun-aroma').click(function() {
    $('.bg-aroma').css({ 'background-image': 'none' });
});
$('#vaniglia').click(function() {
    $('.bg-aroma').css({ 'background-image': 'url(img/bg-vaniglia.png)' });
});
$('#fiori-acqua').click(function() {
    $('.bg-aroma').css({ 'background-image': 'url(img/bg-fiori-acqua.png)' });
});
$('#frutti-rossi').click(function() {
    $('.bg-aroma').css({ 'background-image': 'url(img/bg-frutti-rossi.png)' });
});
$('#tooltip-icon-matt').click(function() {
    $('#tooltip-content-matt').css({ 'opacity': '1' , 'visibility': 'visible' });
});
$('#tooltip-icon-shine').click(function() {
    $('#tooltip-content-shine').css({ 'opacity': '1' , 'visibility': 'visible' });
});
$('#tooltip-icon-kiss').click(function() {
    $('#tooltip-content-kiss').css({ 'opacity': '1' , 'visibility': 'visible' });
});
$('#tooltip-icon-happy').click(function() {
    $('#tooltip-content-happy').css({ 'opacity': '1' , 'visibility': 'visible' });
});
$('#tooltip-icon-lampone').click(function() {
    $('#tooltip-content-lampone').css({ 'opacity': '1' , 'visibility': 'visible' });
});
$('#tooltip-icon-canapa').click(function() {
    $('#tooltip-content-canapa').css({ 'opacity': '1' , 'visibility': 'visible' });
});
$('.icon-close-tooltip').click(function() {
    $('#tooltip-content-matt').css({ 'opacity': '0' , 'visibility': 'hidden' });
    $('#tooltip-content-shine').css({ 'opacity': '0' , 'visibility': 'hidden' });
    $('#tooltip-content-kiss').css({ 'opacity': '0' , 'visibility': 'hidden' });
    $('#tooltip-content-happy').css({ 'opacity': '0' , 'visibility': 'hidden' });
    $('#tooltip-content-lampone').css({ 'opacity': '0' , 'visibility': 'hidden' });
    $('#tooltip-content-canapa').css({ 'opacity': '0' , 'visibility': 'hidden' });
});